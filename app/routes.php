<?php
Route::get('/', 'HomeController@showWelcome');
Route::get('page/{id}','HomeController@getPage');
Route::get('test', 'HomeController@getTest');
Route::get('admin/login','AdminController@getLogin');
Route::post('admin/login','AdminController@postLogin');
Route::when('admin/*', 'csrf', array('post'));
Route::group(array('before' => 'auth'),function(){
	Route::Controller('admin','AdminController');
});