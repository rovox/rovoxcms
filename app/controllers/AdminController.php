<?php

class AdminController extends BaseController {

    protected $layout = 'admin.layouts.master';

    /*
     * index
     */

    public function getIndex() {
        return Redirect::intended('admin/dashboard');
    }

    /*
     * login;
     */

    public function postLogin() {
        try {
            // Login credentials
            $credentials = array(
                'email' => Input::get('email'),
                'password' => Input::get('password'),
            );

            // Authenticate the user
            $user = Sentry::authenticate($credentials, false);
            return Redirect::intended('admin/dashboard');
        } catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
            return Redirect::to('admin/login')->with('message', 'login field is required.');
        } catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
            return Redirect::to('admin/login')->with('message', 'Password field is required.');
        } catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
            return Redirect::to('admin/login')->with('message', 'Username or Password Incorrect.');
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            return Redirect::to('admin/login')->with('message', 'Username or Password Incorrect.');
        } catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
            return Redirect::to('admin/login')->with('message', 'Username or Password Incorrect.');
        }

        // The following is only required if the throttling is enabled
        catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e) {
            echo 'User is suspended.';
        } catch (Cartalyst\Sentry\Throttling\UserBannedException $e) {
            echo 'User is banned.';
        }
    }

    public function getLogin() {
        $this->layout->content = View::make('admin.page.login');
    }

    /*
     * 
     * dashboard
     * 
     */

    public function getDashboard() {
        
    }

    /*
     * 
     * pages
     * 
     */

    public function getPage($page, $id = null) {
        switch ($page) {
            case 'all':
                $pages = Page::all();
                $this->layout->content = View::make('admin.page.listpage')->with('pages', $pages);
                break;
            case 'new';
                $categories = Category::lists('name', 'id');
                $this->layout->content = View::make('admin.page.newpage')->with('categories', $categories);
                break;
            case 'delete':
                try {
                    $page = Page::findorfail($id);
                    $this->layout->content = View::make('admin.page.deletepage')->with('page', $page);
                } catch (Exception $e) {
                    return app::abort(404);
                }
                break;
            case 'edit':
                $categories = Category::lists('name', 'id');
                $page = Page::find($id);
                $this->layout->content = View::make('admin.page.editpage')->with('page', $page)->with('categories', $categories);
                break;
        }
    }

    public function postPage($page, $id = null) {
        switch ($page) {
            case 'new';
                $validator = Validator::make(
                                Input::all(), Page::$rules
                );
                if ($validator->fails()) {
                    return Redirect::to('admin/page/new')->withErrors($validator);
                }
                $page = new Page;
                $page->name = Input::get('name');
                $page->body = Input::get('body');
                $page->tags = 'none';
                $page->pagetype = 'fullwidth';
                $page->category_id = Input::get('category');
                if ($page->save()) {
                    return Redirect::to('admin/page/all')->with('message', 'Your page has been created.');
                } else {
                    return Redirect::to('admin/page/new')->with('message', 'Your page has faild to create.')->withInput();
                }
                break;
            case 'delete':
                $page = Page::findorfail($id);
                if ($page->delete()) {
                    return Redirect::to('admin/page/all')->with('message', 'Your page has deleted.');
                } else {
                    return Redirect::to('admin/page/delete/' . $id)->with('message', 'Your page faild to delete.');
                }
                break;
            case 'edit':
                $validator = Validator::make(
                                Input::all(), Page::$rules
                );
                if ($validator->fails()) {
                    return Redirect::to('admin/page/edit' . $id)->withErrors($validator);
                }
                $page = Page::findorfail($id);
                $page->name = Input::get('name');
                $page->body = Input::get('body');
                $page->tags = 'none';
                $page->pagetype = 'fullwidth';
                $page->category_id = Input::get('category');
                if ($page->save()) {
                    return Redirect::to('admin/page/all')->with('message', 'Your page has been updated.');
                } else {
                    return Redirect::to('admin/page/edit/' . $id)->with('message', 'Your page has faild to update.')->withInput();
                }
                break;
        }
    }

    /*
     * 
     * Settings
     * 
     */

    public function getSetting($page, $id = null) {
        switch ($page) {
            case "all":
                $settings = Setting::all();
                $this->layout->content = View::make('admin.page.listsetting')->with('settings', $settings);
                break;
            case 'new':
                $this->layout->content = View::make('admin.page.newsetting');
                break;
            case 'delete':
                try {
                    $setting = Setting::findorfail($id);
                    $this->layout->content = View::make('admin.page.deletesetting')->with('setting', $setting);
                } catch (Exception $e) {
                    return app::abort(404);
                }
                break;
            case 'edit':
                $setting = Setting::find($id);
                $this->layout->content = View::make('admin.page.editsetting')->with('setting', $setting);
                break;
        }
    }

    public function postSetting($page, $id = null) {
        switch ($page) {
            case 'new':
                $validator = Validator::make(
                                Input::all(), Setting::$rules
                );
                if ($validator->fails()) {
                    return Redirect::to('admin/setting/new')->withErrors($validator);
                }
                $setting = new Setting;
                $setting->name = Input::get('name');
                $setting->value = Input::get('value');
                if ($setting->save()) {
                    return Redirect::to('admin/setting/all')->with('message', 'Your setting has been created.');
                } else {
                    return Redirect::to('admin/setting/new')->with('message', 'Your setting has faild to create.')->withInput();
                }
                break;
            case 'delete':
                $setting = Setting::findorfail($id);
                if ($setting->delete()) {
                    return Redirect::to('admin/setting/all')->with('message', 'Your setting has been deleted.');
                } else {
                    return Redirect::to('admin/setting/delete/' . $id)->with('message', 'Your setting faild to delete.');
                }
                break;
            case 'edit':
                $validator = Validator::make(
                                Input::all(), Setting::$rules
                );
                if ($validator->fails()) {
                    return Redirect::to('admin/setting/edit' . $id)->withErrors($validator);
                }
                $setting = Setting::findorfail($id);
                $setting->name = Input::get('name');
                $setting->value = Input::get('value');
                if ($setting->save()) {
                    return Redirect::to('admin/setting/all')->with('message', 'Your setting has been updated.');
                } else {
                    return Redirect::to('admin/setting/edit/' . $id)->with('message', 'Your setting has faild to update.')->withInput();
                }
                break;
        }
    }

    /*
     *
     * Logout.
     *
     *
     */

    public function getLogout() {
        Sentry::logout();
        return Redirect::to('admin/login')->with('message', 'You have logged out.');
    }

    /*
     * 
     * categories
     * 
     */

    public function getCategory($page, $id = null) {
        switch ($page) {
            case 'all':
                $categories = Category::all();
                $this->layout->content = View::make('admin.page.listcategory')->with('categories', $categories);
                break;
            case 'new';
                $this->layout->content = View::make('admin.page.newcategory');
                break;
            case 'delete':
                try {
                    $category = Category::findorfail($id);
                    $this->layout->content = View::make('admin.page.deletecategory')->with('category', $category);
                } catch (Exception $e) {
                    return app::abort(404);
                }
                break;
            case 'edit':
                $category = Category::find($id);
                $this->layout->content = View::make('admin.page.editcategory')->with('page', $page)->with('category', $category);
                break;
        }
    }

    public function postCategory($page, $id = null) {
        switch ($page) {
            case 'new':
                $validator = Validator::make(
                                Input::all(), Category::$rules
                );
                if ($validator->fails()) {
                    return Redirect::to('admin/category/new')->withErrors($validator)->withInput();
                }
                $setting = new Category;
                $setting->name = Input::get('name');
                $setting->description = Input::get('description');
                if ($setting->save()) {
                    return Redirect::to('admin/category/all')->with('message', 'Your category has been created.');
                } else {
                    return Redirect::to('admin/category/new')->with('message', 'Your category has faild to create.')->withInput();
                }
                break;
            case 'delete':
                $category = Category::findorfail($id);
                $count = $category->pages()->count();
                if(!empty($count)){
                    return Redirect::to('admin/category/all')->with('message', 'Your category is not empty.'); 
                }
                if ($category->delete()) {
                    return Redirect::to('admin/category/all')->with('message', 'Your category has been deleted.');
                } else {
                    return Redirect::to('admin/category/delete/' . $id)->with('message', 'Your category faild to delete.');
                }
                break;
            case 'edit':
                $validator = Validator::make(
                                Input::all(), Setting::$rules
                );
                if ($validator->fails()) {
                    return Redirect::to('admin/category/edit' . $id)->withErrors($validator);
                }
                $category = Category::findorfail($id);
                $category->name = Input::get('name');
                $category->description = Input::get('description');
                if ($category->save()) {
                    return Redirect::to('admin/category/all')->with('message', 'Your setting has been updated.');
                } else {
                    return Redirect::to('admin/category/edit/' . $id)->with('message', 'Your setting has faild to update.')->withInput();
                }
                break;
        }
    }


    /*
     * menu
     */
    public function getMenu($page, $id = null){
        
        switch($page){
            case 'all':
                $menu = Menu::all();
                $this->layout->content = View::make('admin.page.listmenu')->with('menus' , $menu);
                break;
            case 'new':
                $parent = array('' => 'none') + Menu::tree()->lists('title', 'id');
                $this->layout->content = View::make('admin.page.newmenu')->with('parent' , $parent);              
                break;
            case 'edit':
                $parent = array('' => 'none') + Menu::tree()->lists('title', 'id');
                $menu = Menu::find($id);
                $this->layout->content = View::make('admin.page.editmenu')->with('parent' , $parent)->with('menu',$menu);              
                break;
            case 'delete':
                $menu = Menu::find($id);
                $this->layout->content = View::make('admin.page.deletemenu')->with('menu' , $menu);
                break;
        }
    }
    
    public function postMenu($page, $id = null){
        switch($page){
            case 'new':
                $validator = Validator::make(
                                Input::all(), Menu::$rules
                );
                if ($validator->fails()) {
                    return Redirect::to('admin/menu/new')->withErrors($validator)->withInput();
                }
                $menu = new Menu;
                $menu->title = Input::get('title');
                $menu->link = Input::get('link');
                $menu->parent_id = Input::has('parent') && Input::get('parent') != '' ? Input::get('parent') : null;
                if ($menu->save()) {
                    return Redirect::to('admin/menu/all')->with('message', 'Your menu has been created.');
                } else {
                    return Redirect::to('admin/menu/new')->with('message', 'Your menu has faild to create.')->withInput();
                }
                break;
            case 'edit':
                $validator = Validator::make(
                                Input::all(), Menu::$rules
                );
                if ($validator->fails()) {
                    return Redirect::to('admin/category/edit' . $id)->withErrors($validator);
                }
                $menu = Menu::findorfail($id);
                $menu->title = Input::get('title');
                $menu->link = Input::get('link');
                $menu->parent_id = Input::has('parent') && Input::get('parent') != '' ? Input::get('parent') : null;
                if ($menu->save()) {
                    return Redirect::to('admin/menu/all')->with('message', 'Your menu has been updated.');
                } else {
                    return Redirect::to('admin/menu/edit/' . $id)->with('message', 'Your menu has faild to update.')->withInput();
                }
                break;
            case 'delete':
                $items = Menu::find($id)->children;
                if(!empty($items)){
                    foreach($items as $item){
                        $menu = Menu::find($item->id);
                        $menu->delete();
                    }
                }
                $menuitem = Menu::find($id);
                if($menuitem->delete()){
                    return Redirect::to('admin/menu/all')->with('message', 'Your menuitem has been deleted.');
                }else{
                                    if($menuitem->delete()){
                    return Redirect::to('admin/menu/all')->with('message', 'Your menuitem has faild delete please try again..');  
                }
                break;
        }
    }
}
}
