<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
class HomeController extends BaseController {
	
	protected $layout = 'layouts.master';

	public function showWelcome()
	{
                $page = Page::where('id','=',Setting::get('site.homepageid'))->first();
		$this->layout->content = View::make('page.homepage')->with('page',$page);
	}
        public function getPage($id){
            try{
                $page = Page::findorfail($id);
            } catch (ModelNotFoundException $ex) {
                app::abort(404, 'Page not found.');
            }
	    $this->layout->content = View::make('page.'. $page->pagetype)->with('page', $page);
        }
	public function getTest(){
		echo"<pre>";
		print_r(Page::find(1)->category);
		echo "</pre>";
	}

}
