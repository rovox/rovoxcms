@section('content')
    <div class="container" style="padding-bottom: 20px;">
        <h3>Delete Menuitem:</h3>
        <p>Your about to delete {{$menu->title}} <b><u>and all the submenu items</u></b></p>
        <div class="col-md-1">
        {{Link_to('/admin/menu/all','go back',array('class'=>'btn btn-success'))}}    
        </div>
        <div class="col-md-1">
            {{Form::open()}}
            {{Form::submit('Delete menu',array('class' => 'btn btn-danger'))}}
            {{Form::close()}}
        </div>
    </div>
@stop