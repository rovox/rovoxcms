@section('content')
    <div class="container img-rounded">
        <h3>New page:</h3>
        <hr>
        {{Form::open()}}
        <div class="row">
            <div class="col-md-8">
                    {{form::Label('name','Page name:',array('class'=>'control-label'))}}
                    <b class='red'>{{$errors->first('name')}}</b>
                    {{Form::text('name', null,array('class'=>'form-control','placeholder'=>'Title'))}}
                    {{form::Label('body','Body content:',array('class'=>'control-label'))}}
                    <b class='red'>{{$errors->first('body')}}</b>
                    {{Form::textarea('body', null,array('class'=>'form-control','placeholder'=>'page body'))}}
                    <br>
                    {{Form::submit('Create page',array('class'=>'btn btn-success btn-lg'))}}
            </div>
            <div class="col-md-4">
                {{form::Label('category','category name:',array('class'=>'control-label'))}}
                {{ Form::select('category', $categories,null,array('class'=>'form-control'))}}
            </div>
        </div>
        {{Form::close()}}
    </div>
@stop()