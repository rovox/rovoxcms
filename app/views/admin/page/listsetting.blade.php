@section('content')
    <div class="container">
        <h3>All settings:</h3>
        {{ Link_to('admin/setting/new','New setting',array('class'=>'btn btn-primary')) }}
         <table class="table table-hover">
            <tr>
              <td>name</td>
              <td>value</td>
              <td></td>
            </tr>
            @foreach($settings as $setting)
            <tr>
              <td>{{ $setting->name }}</td>
              <td>{{ $setting->value }}</td>
              <td><center>{{ link_to('admin/setting/edit/' . $setting->id,'edit',array('class'=>'btn btn-warning btn-sm')) }} {{ link_to('admin/setting/delete/' . $setting->id,'delete',array('class'=>'btn btn-warning btn-sm')) }}</center></td>
            </tr>
            @endforeach()
        </table>    
    </div>
@stop()