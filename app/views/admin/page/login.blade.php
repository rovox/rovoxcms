@section('content')
        <div class="col-md-12">
            <div class="modal-dialog" style="margin-bottom:0">
                <div class="modal-content">
                            <div class="panel-heading">
                                <h3>Rovox content Manager: Login</h3>
                                    <p>Welcome at Rovox Content Manager Please use a vaild username and password to login.</p>
                            </div>
                            <div class="panel-body">
                                {{Form::open(array('role'=>'form'))}}
                                    <fieldset>
                                        <div class="form-group">
                                            {{Form::text('email',null,array('class'=>'form-control','placeholder'=>'E-mail','autofocus'=>''))}}
                                        </div>
                                        <div class="form-group">
                                            {{Form::password('password',array('class'=>'form-control','placeholder'=>'Password','autofocus'=>''))}}
                                        </div>
                                        <!-- Change this to a button or input when using this as a form -->
                                        {{Form::submit('Login',array('class'=>'btn btn-sm btn-success'))}}
                                    </fieldset>
                                {{Form::close()}}
                            </div>
                        </div>
            </div>
        </div>
    </div>
@stop