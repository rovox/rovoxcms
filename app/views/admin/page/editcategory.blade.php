@section('content')
<div class="container img-rounded" style="padding-bottom: 25px;">
    <h3>Update category:</h3>
    <hr>
    {{Form::open()}}
    <div class="row">
        <div class="col-md-3">
            {{form::Label('name','category name:',array('class'=>'control-label'))}}
            <b class='red'>{{$errors->first('name')}}</b>
            {{Form::text('name', $category->name,array('class'=>'form-control','placeholder'=>'Name'))}}
            <br>
        </div>
        <div class="col-md-3">
            {{form::Label('description','description:',array('class'=>'control-label'))}}
            <b class='red'>{{$errors->first('description')}}</b>
            {{Form::text('description', $category->description,array('class'=>'form-control','placeholder'=>'description'))}}
        </div>
    </div>
    {{Form::submit('Update category',array('class'=>'btn btn-success btn-lg'))}}
    {{Form::close()}}
</div>
@stop()