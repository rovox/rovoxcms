@section('content')
    <div class="container">
        <h3>All pages:</h3>
        {{ Link_to('admin/page/new','New page',array('class'=>'btn btn-primary')) }}
         <table class="table table-hover">
            <tr>
              <td>name</td>
              <td>Created at:</td>
              <td>Category</td>
              <td></td>
            </tr>
            @foreach($pages as $page)
            <tr>
              <td>{{ $page->name }}</td>
              <td>{{ $page->created_at }}</td>
              <td>{{ Page::find($page->id)->category()->first()->name }}</td>
              <td><center>{{ link_to('admin/page/edit/' . $page->id,'edit',array('class'=>'btn btn-warning btn-sm')) }} {{ link_to('admin/page/delete/' . $page->id,'delete',array('class'=>'btn btn-warning btn-sm')) }}</center></td>
            </tr>
            @endforeach()
        </table> 
        
    </div>
    
@stop()