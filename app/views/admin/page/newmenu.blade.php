@section('content')
    <div class="container img-rounded">
        <h3>New Menu item:</h3>
        <hr>
        {{Form::open()}}
        <div class="row">
            <div class="col-md-8">
                    {{form::Label('title','Title:',array('class'=>'control-label'))}}
                    <b class='red'>{{$errors->first('title')}}</b>
                    {{Form::text('title', null,array('class'=>'form-control','placeholder'=>'Title'))}}
                    {{form::Label('link','Link:',array('class'=>'control-label'))}}
                    <b class='red'>{{$errors->first('link')}}</b>
                    {{Form::text('link', null,array('class'=>'form-control','placeholder'=>'item link'))}}
                    <br>
                    {{Form::submit('Create page',array('class'=>'btn btn-success btn-lg'))}}
            </div>
            <div class="col-md-4">
                {{form::Label('parent','Parent:',array('class'=>'control-label'))}}
                {{ Form::select('parent', $parent,null,array('class'=>'form-control'))}}
            </div>
        </div>
        {{Form::close()}}
    </div>
@stop()