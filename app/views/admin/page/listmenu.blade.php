@section('content')
    <div class="container">
        <h3>All menuitems:</h3>
        {{ Link_to('admin/menu/new','New Menu',array('class'=>'btn btn-primary')) }}
         <table class="table table-hover table-bordered">
            <tr>
              <td>Name</td>
              <td>Link</td>
              <td>Parent</td>
              <td></td>
            </tr>
            @foreach($menus as $menu)
            <tr>
              <td>{{ $menu->title }}</td>
              <td>{{ $menu->link }}</td>
              <td>
                @if(!is_null($menu->parent_id)) 
                    {{ Menu::find($menu->parent_id)->title }} 
                @endif
              </td>
              <td><center>{{ link_to('admin/menu/edit/' . $menu->id,'edit',array('class'=>'btn btn-warning btn-sm')) }} {{ link_to('admin/menu/delete/' . $menu->id,'delete',array('class'=>'btn btn-warning btn-sm')) }}</center></td>
            </tr>
            @endforeach()
        </table> 
        
    </div>
    
@stop()