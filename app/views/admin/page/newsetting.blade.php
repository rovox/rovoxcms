@section('content')
<div class="container img-rounded" style="padding-bottom: 25px;">
    <h3>New Setting:</h3>
    <hr>
    {{Form::open()}}
    <div class="row">
        <div class="col-md-3">
            {{form::Label('name','setting name:',array('class'=>'control-label'))}}
            <b class='red'>{{$errors->first('name')}}</b>
            {{Form::text('name', Input::old('name'),array('class'=>'form-control','placeholder'=>'Name'))}}
            <br>
        </div>
        <div class="col-md-3">
            {{form::Label('value','value:',array('class'=>'control-label'))}}
            <b class='red'>{{$errors->first('description')}}</b>
            {{Form::text('value', Input::old('value'),array('class'=>'form-control','placeholder'=>'Value'))}}
        </div>
    </div>
    {{Form::submit('Create Setting',array('class'=>'btn btn-success btn-lg'))}}
    {{Form::close()}}
</div>
@stop()