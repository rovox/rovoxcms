@section('content')
    <div class="container" style="padding-bottom: 20px;">
        <h3>Delete page:</h3>
        <p>Your about to delete {{$page->name}}</p>
        <div class="col-md-1">
        {{Link_to('/admin/page/all','go back',array('class'=>'btn btn-success'))}}    
        </div>
        <div class="col-md-1">
            {{Form::open()}}
            {{Form::submit('Delete page',array('class' => 'btn btn-danger'))}}
            {{Form::close()}}
        </div>
    </div>
@stop