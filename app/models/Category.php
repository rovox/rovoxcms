<?php

class Category extends Eloquent{
    protected $table = 'category';
    
    public static $rules = array(
        'name'=>'required',
        'description'=>'required'
    );
    
    public function pages(){
        return $this->hasmany('Page');
    }
}
