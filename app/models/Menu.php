<?php

class Menu extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menu';

    public static $rules = array(
        'title'=>'required',
        'link'=>'required'
    );
    
    public function parent() {

        return $this->hasOne('menu', 'id', 'parent_id');

    }

    public function children() {

        return $this->hasMany('menu', 'parent_id', 'id');

    }  

    public static function tree() {

        return static::with(implode('.', array_fill(0, 100, 'children')))->where('parent_id', '=', NULL)->get();

    }

}
