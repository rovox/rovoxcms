<?php

class Page extends Eloquent {
    protected $table = 'page';
    
    public static $rules = array(
        'name'=>'required',
        'body'=>'required'
    );
    public function Category(){
        return $this->belongsTo('Category');
    }
}