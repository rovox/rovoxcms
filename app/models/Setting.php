<?php


class Setting extends Eloquent{
    protected $table = 'setting';
    
    public static $rules = array(
        'name'=>'required',
        'value'=>'required'
    );
    public static function get($setting){
        $getsetting = Setting::where('name','=',$setting)->first();
        if(Setting::where('name','=',$setting)->count()){
            return $getsetting->value;
        }else{
            throw new Exception('Setting ' . $setting . ' not found.');
        }
    }
}
