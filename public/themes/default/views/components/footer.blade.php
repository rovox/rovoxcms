    <footer class="footer">
      <div class="container">
        <p class="text-muted">&copy; {{ Setting::get('site.name') }}</p>
      </div>
    </footer>